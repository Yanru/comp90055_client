//
//  RunningHelper.swift
//  Running
//
//  Created by zita on 16/3/7.
//  Copyright © 2016年 Zita. All rights reserved.
//

import Foundation

typealias CancelableTask = (cancel: Bool) -> Void

extension String {
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}

extension Double {
    func format(f:String) ->String {
        return String(format:"%\(f)f",self)
    }
}

//Format the run duration
func stringSecondCount(second:NSInteger) ->String {
    var remainingSeconds = second
    let hours = NSInteger(remainingSeconds / 3600)
    remainingSeconds = remainingSeconds - hours * 3600
    let minutes = remainingSeconds / 60
    remainingSeconds = remainingSeconds - minutes * 60
    
    if hours > 0 {
        return String(format:"%02i:%02i:%02i",hours,minutes,remainingSeconds)
    }else if minutes > 0 {
        return String(format:"%02i:%02i",minutes,remainingSeconds)
    }else {
        return String(format:"00:%02i",remainingSeconds)
    }
}

//Calculate run speed
func stringAvgPace(distance: Double,time: NSInteger) ->String {
    if distance == 0 || time == 0 {
        return "00:00"
    }
    
    let speed = distance/Double(time)*3.6
    
    return String(format:"%.2fKm/h",speed)
}

//Calculate calories burned
func calculateCal(distance:Double,time:NSInteger) ->Double {
    let speed = Double(time)/distance*40.0/6.0; //convert the measure into min/400 meters
    let weight = NSUserDefaults.standardUserDefaults().stringForKey(UserDefault().physiological.weight)
    return Double(weight!)!*30.0/speed*Double(time)/3600.0;
    //Calories (kCal)＝ weight (kg) × duration（hour）× K  where K＝30 / speed (min/400 meters)
}

//Local database
func dataFilePath() ->String{
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true) as Array
    let documentsDirectory = paths[0] as NSString
    return documentsDirectory.stringByAppendingPathComponent("data.plist")
}

func randomInRange(range: Range<Int>) -> Int {
    let count = UInt32(range.endIndex - range.startIndex)
    return  Int(arc4random_uniform(count)) + range.startIndex
}

func delay(time: NSTimeInterval, work: dispatch_block_t) -> CancelableTask? {
    
    var finalTask: CancelableTask?
    
    let cancelableTask: CancelableTask = { cancel in
        if cancel {
            finalTask = nil // key
            
        } else {
            dispatch_async(dispatch_get_main_queue(), work)
        }
    }
    
    finalTask = cancelableTask
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(time * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
        if let task = finalTask {
            task(cancel: false)
        }
    }
    
    return finalTask
}

func cancel(cancelableTask: CancelableTask?) {
    cancelableTask?(cancel: true)
}