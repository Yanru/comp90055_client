//
//  RunModel.swift
//  Running
//
//  Created by zita on 16/3/10.
//  Copyright © 2016 Zita. All rights reserved.
//

import Foundation

struct UserDefault {
    let name = "name"
    let physiological = Physiological()
    let token = "token"
}

struct Physiological {
    let weight = "weight"
    let height = "height"
    let gender = "gender"
}

struct Run {
    var date:String
    var distance:Double
    var time:NSInteger
    var cal:String
    var locations:[location]

    init(distance:Double,time:NSInteger,locations:[location],date:String,cal:String){
        self.distance = distance
        self.time = time
        self.locations = locations
        self.date = date
        self.cal = cal
    }
}

class location {
    var altitude:Double 
    var latitude: Double 
    var longitude: Double 
    var perdistance:Double
    init(altitude:Double,latitude:Double,longitude:Double,perdistance:Double){
        self.altitude = altitude
        self.latitude = latitude
        self.longitude = longitude
        self.perdistance = perdistance
    }
}
