//
//  CountDownView.swift
//  Running
//
//  Created by zita on 16/3/7.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit

class CountDownView: UIView {
    
    var countDownNumber = 0
    
    var countDownLabel:UILabel!
    var countDownCurrentNumber = 0
    var timer = NSTimer()
    
    weak var delegate:CountDownViewDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    deinit {
        print("deinit")
    }
    
    func setup() {
        opaque = false
        backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.5)
        
        countDownLabel = UILabel(frame: CGRectMake(0, 0, frame.size.width+400, frame.size.height))
        countDownLabel!.center = center
        countDownLabel!.alpha = 1
        countDownLabel!.textColor = UIColor.blackColor()
        countDownLabel!.textAlignment = .Center
        countDownLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: frame.size.width*0.3)
        addSubview(countDownLabel!)
    }
    
    func start(){
        countDownCurrentNumber = countDownNumber
        countDownLabel.text = String(stringInterpolationSegment: countDownNumber)
        animate()
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(CountDownView.animate), userInfo: nil, repeats: true)
    }
    
    func stop() {
        timer.invalidate()
    }
    
    func animate() {
        //Countdown Timer before starting a run
        UIView.animateWithDuration(0.8, animations: { () -> Void in
            let transform = CGAffineTransformMakeScale(2.5, 2.5)
            self.countDownLabel.transform = transform
            self.countDownLabel.alpha = 0
            }) { (Bool) -> Void in
                if Bool{
                    if self.countDownCurrentNumber == 0 {
                        self.stop()
                        self.delegate?.countDownFinished?()
                    }else {
                        self.countDownLabel.transform = CGAffineTransformIdentity
                        self.countDownLabel.alpha = 1
                        self.countDownCurrentNumber -= 1
                        if self.countDownCurrentNumber == 0 {
                            self.countDownLabel.text = "RUN"
                        }else {
                            self.countDownLabel.text = String(stringInterpolationSegment: self.countDownCurrentNumber)
                        }
                    }
                }
        }
    }
    
}

@objc protocol CountDownViewDelegate {
   optional func countDownFinished()
}
