//
//  DistanceAnnotation.swift
//  Running
//
//  Created by zita on 16/3/12.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import MapKit

class DistanceAnnotation: NSObject,MKAnnotation {
    
    var imageName:String?
    var image:UIImage?
    
    var myCoordinate: CLLocationCoordinate2D
    
    init(myCoordinate: CLLocationCoordinate2D) {
        self.myCoordinate = myCoordinate
    }
    
    var coordinate: CLLocationCoordinate2D {
        return myCoordinate
    }
    
}
