//
//  LeftDrawVC.swift
//  Running
//
//  Created by zita on 30/05/2016.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit

class LeftDrawVC: UITableViewController {

    @IBOutlet weak var usernameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        usernameLabel.text = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().name) as? String

    }

    @IBAction func logoutPressed(sender: AnyObject) {
        let login = UIStoryboard(name: "Login", bundle: NSBundle.mainBundle()).instantiateInitialViewController()
        NSUserDefaults.standardUserDefaults().removeObjectForKey(UserDefault().token)
        presentViewController(login!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
