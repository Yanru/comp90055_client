//
//  LoginViewController.swift
//  Running
//
//  Created by zita on 30/05/2016.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class LoginViewController: UITableViewController {

    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!

    let userDefault = UserDefault()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.lightGrayColor()
        username.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitButtonPressed(button: UIButton) {
        if ((username.text == "") || (password.text) == "") {
            let alertController = UIAlertController(title: "Error", message: "Please fill in username and password.", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
            return
        }

        if password.text?.characters.count < 6 || password.text?.characters.count > 20 {
            let alertController = UIAlertController(title: "Error", message: "Invalid password.", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
            return
        }

        MBProgressHUD.showHUDAddedTo(view, animated: true)
        Alamofire.request(.POST, "https://r.nsswift.com/api/user/login", parameters: ["username": username.text!, "password": password.text!], encoding: ParameterEncoding.JSON, headers: nil).responseJSON { (response) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            switch response.result {
            case .Success(let value):
                if let code = value["code"] as? Int {
                    if code == 200 {
                        NSUserDefaults.standardUserDefaults().setInteger(value["data"]!!["gender"] as! Int, forKey: self.userDefault.physiological.gender)
                        NSUserDefaults.standardUserDefaults().setObject(value["data"]!!["height"], forKey: self.userDefault.physiological.height)
                        NSUserDefaults.standardUserDefaults().setObject(value["data"]!!["weight"], forKey: self.userDefault.physiological.weight)
                        NSUserDefaults.standardUserDefaults().setObject(value["token"], forKey: self.userDefault.token)
                        NSUserDefaults.standardUserDefaults().setObject(value["data"]!!["username"], forKey: self.userDefault.name)
                        let mainVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateInitialViewController()
                        self.presentViewController(mainVC!, animated: true, completion: nil)
                    }else {
                        let alertController = UIAlertController(title: "Failed", message: "Username or password is invalid, please try  again.", preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        return
                    }
                }

            case .Failure(_):
                let alertController = UIAlertController(title: "Failed", message: "No network connection.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}
