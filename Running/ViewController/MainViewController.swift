//
//  MainViewController.swift
//  Running
//
//  Created by zita on 16/3/6.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class MainViewController: UIViewController {
    
    @IBOutlet weak var totalRuns: UILabel!
    @IBOutlet weak var totalCal: UILabel!
    @IBOutlet weak var totalKM: UILabel!
    var totalKMAmount: Double = 0
    var totalRunTimes: Int = 0
    var totalCalAmount: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
 
        let leftButton = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.showRunDetailView), name: "RunningViewDismissed", object: nil)

        loadWebData()
    }

    func loadWebData() {
        MBProgressHUD.showHUDAddedTo(view, animated: true)
        let token = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().token) as! String
        Alamofire.request(.GET, "https://r.nsswift.com/api/running/getall", parameters: nil, encoding: ParameterEncoding.JSON, headers: ["x-access-token": token]).responseJSON { (response) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            switch response.result {
            case .Success(let value):
                if let data = value["data"] as? NSArray {
                    self.totalRunTimes = data.count
                    for item in data {
                        if let runDic = item as? NSDictionary {
                            let distance = runDic["distance"] as! Double
                            self.totalKMAmount += distance
                            let cal = String(runDic["cal"]!)

                            self.totalCalAmount += Double(cal) ?? 0
                        }
                    }

                    dispatch_async(dispatch_get_main_queue(), {
                        //Format total distance from meter into kilometer
                        let dis = (self.totalKMAmount/1000).format(self.totalKMAmount/1000 > 10.0 ?".1":".2")
                        self.totalCal.text = String(self.totalCalAmount)
                        self.totalKM.text = dis
                        self.totalRuns.text = String(self.totalRunTimes)
                    })
                }
                print(value)
            case .Failure(_):
                let alertController = UIAlertController(title: "Failed", message: "No network connection.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func beginRunPressed(sender: AnyObject) {
        let runDic  =  NSDictionary(contentsOfFile: dataFilePath()) as? [String:AnyObject]
        if runDic == nil {
            performSegueWithIdentifier("beginRunSegue", sender: nil)
            return
        }

        let distance = runDic!["distance"] as! Double
        let time = runDic!["time"] as! NSInteger
        let cal = runDic!["cal"] as! String

        let array :[AnyObject] = (runDic!["locations"])! as! [AnyObject]

        var locationArray: [Dictionary<String, Double>] = []
        for dic in array {
            let altitude = dic["altitude"] as! Double 
            let latitude = dic["latitude"] as! Double 
            let longitude = dic["longitude"] as! Double 
            let perdistance = dic["perdistance"] as! Double
            let dict = ["altitude": altitude, "latitude": latitude, "perdistance": perdistance, "longitude": longitude]
            locationArray.append(dict)
        }

        var parameters: [String: AnyObject] = ["distance": distance, "time": time, "cal": cal]

        parameters["location"] = locationArray

        MBProgressHUD.showHUDAddedTo(view, animated: true)
        let token = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().token) as! String
        Alamofire.request(.POST, "https://r.nsswift.com/api/running/add", parameters: parameters, encoding: ParameterEncoding.JSON, headers: ["x-access-token": token, "Content-Type": "application/json"]).responseJSON { (response) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            switch response.result {
            case .Success(_):
                self.deleteLocalFile()
                self.performSegueWithIdentifier("beginRunSegue", sender: nil)
            case .Failure(_):
                let alertController = UIAlertController(title: "Alert", message: "Starting the run will erase the existing run record. Sure?", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                alertController.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action) in
                    self.deleteLocalFile()
                    self.performSegueWithIdentifier("beginRunSegue", sender: nil)
                }))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }

    func deleteLocalFile() {
        let filePath = dataFilePath()
        if NSFileManager.defaultManager().fileExistsAtPath(filePath) {
            try! NSFileManager.defaultManager().removeItemAtPath(filePath)
        }
    }

    func showRunDetailView(){
        loadWebData()
    }
}
