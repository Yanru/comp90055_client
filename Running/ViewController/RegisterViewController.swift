//
//  RegisterViewController.swift
//  Running
//
//  Created by zita on 30/05/2016.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class RegisterViewController: UITableViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    var gender = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.lightGrayColor()
        username.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func registerNewUser(name: String, password: String, weight: Double, height: Double, gentle: Bool) {

        Alamofire.request(.POST, "https://r.nsswift.com/api/user/add", parameters: ["username": "a", "password": "p", "height": 160, "weight": 65, "genter": 1], encoding: ParameterEncoding.JSON, headers: nil).responseJSON { (response) in
            print(response)
        }
    }


    @IBAction func genderButtonPressed(sender: UIButton) {

        maleButton.selected = sender == maleButton
        femaleButton.selected = sender == femaleButton

        let selectColor = UIColor(red: 75.0/255.0, green: 149.0/255.0, blue: 249.0/255.0, alpha: 1)
        let unselectColor = UIColor.whiteColor()
        gender = sender == maleButton ? 1 : 2

        maleButton.backgroundColor = sender == maleButton ? selectColor : unselectColor
        femaleButton.backgroundColor = sender == femaleButton ? selectColor : unselectColor

        let selectTitleColor = UIColor.whiteColor()
        let unselectTitleColor = UIColor.lightGrayColor()
        maleButton.setTitleColor(selectTitleColor, forState: .Selected)
        maleButton.setTitleColor(unselectTitleColor, forState: .Normal)

        femaleButton.setTitleColor(selectTitleColor, forState: .Selected)
        femaleButton.setTitleColor(unselectTitleColor, forState: .Normal)
    }


    @IBAction func submitButtonPressed(button: UIButton) {
        if ((username.text == "") || (password.text) == "" || (height.text == "") || (weight.text == "")) {
            let alertController = UIAlertController(title: "Error", message: "Please fill in all required information.", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
            return
        }

        if password.text?.characters.count < 6 || password.text?.characters.count > 20 {
            let alertController = UIAlertController(title: "Error", message: "Password length is too short.", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
            return
        }

        MBProgressHUD.showHUDAddedTo(view, animated: true)
        Alamofire.request(.POST, "https://r.nsswift.com/api/user/add", parameters: ["username": username.text!, "password": password.text!, "height": height.text!, "weight": weight.text!, "gender": gender], encoding: ParameterEncoding.JSON, headers: nil).responseJSON { (response) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            switch response.result {
            case .Success(let value):
                if let code = value["code"] as? NSInteger {
                    if code == 200 {
                        NSUserDefaults.standardUserDefaults().setObject(self.username.text, forKey: UserDefault().name)
                        NSUserDefaults.standardUserDefaults().setObject(self.gender, forKey: UserDefault().physiological.gender)
                        NSUserDefaults.standardUserDefaults().setObject(self.height.text, forKey: UserDefault().physiological.height)
                        NSUserDefaults.standardUserDefaults().setObject(self.weight.text, forKey: UserDefault().physiological.weight)
                        NSUserDefaults.standardUserDefaults().setObject(value["token"], forKey: UserDefault().token)
                        let mainVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateInitialViewController()
                        self.presentViewController(mainVC!, animated: true, completion: nil)

                    }
                    else {
                        let alertController = UIAlertController(title: "Failed", message: "This username is used, please enter a different one.", preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        return
                    }
                }
            case .Failure(_):
                let alertController = UIAlertController(title: "Failed", message: "No network connection.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)

            }
        }

    }
    
}


extension RegisterViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == weight {
            if let w = Double(weight.text!) {
                if w > 100 || w < 40 {
                    let alertController = UIAlertController(title: "Error", message: "Please enter a valid weight", preferredStyle: .Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    weight.text = ""
                }
            }
        }

        if textField == height {
            if let w = Double(height.text!) {
                if w > 250 || w < 100 {
                    let alertController = UIAlertController(title: "Error", message: "Please enter a valid height", preferredStyle: .Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    height.text = ""
                }
            }
        }
    }
}
