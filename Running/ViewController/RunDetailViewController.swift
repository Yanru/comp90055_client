//
//  RunDetailViewController.swift
//  Running
//
//  Created by zita on 16/3/11.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import MapKit

class RunDetailViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avgPaceLabel: UILabel!
    @IBOutlet weak var calLabel: UILabel!

    var runDic:[String:AnyObject]?
    var run:Run?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if run == nil {
             converntModel()
        }
        
        showLabel()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        mapView.region = getMaoRegi()
        mapView.addOverlays(arrangeOverlays())
        mapView.addAnnotations(arrangeAnnotations())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   //MARK:Private
    
    func showLabel() {
        //Format run distance from meter to kilometer
        let dis = (self.run!.distance/1000).format(self.run!.distance/1000 > 10.0 ?".1":".2")
        distanceLabel.text = dis
        
        timeLabel.text = stringSecondCount(self.run!.time)
        
        avgPaceLabel.text = stringAvgPace(self.run!.distance, time: self.run!.time)
        
        //calories burned
        calLabel.text = self.run!.cal
    }
    
    
    func converntModel() {
        
        runDic  =  NSDictionary(contentsOfFile: dataFilePath()) as? [String:AnyObject]
        
        let distance = runDic!["distance"] as! Double
        let time = runDic!["time"] as! NSInteger
        let date = runDic!["date"] as! String
        let cal = runDic!["cal"] as! String
        
        let array :[AnyObject] = (runDic?["locations"])! as! [AnyObject]
        var tempArray = [location]()
        
        for dic in array {
            let altitude = dic["altitude"] as! Double 
            let latitude = dic["latitude"] as! Double
            let longitude = dic["longitude"] as! Double
            let perdistance = dic["perdistance"] as! Double
            let loc = location(altitude: altitude, latitude: latitude, longitude: longitude, perdistance: perdistance)
            tempArray.append(loc)
        }
        
        run = Run(distance: distance, time: time, locations: tempArray, date: date, cal: cal)
    }
    
    //Set the display boundary on map
    func getMaoRegi() ->MKCoordinateRegion {
        
        var region = MKCoordinateRegion()
        
        let initialLoc = (run?.locations.first)! as location
        
        var minLat = initialLoc.latitude
        var minLng = initialLoc.longitude
        var maxLat = initialLoc.latitude
        var maxLng = initialLoc.longitude
        
        for loc in (run?.locations)! {
            if loc.latitude < minLat {
                minLat = loc.latitude
            }
            if loc.longitude < minLng {
                minLng = loc.longitude
            }
            if loc.latitude > maxLat {
                maxLat = loc.latitude
            }
            if loc.longitude > maxLng {
                maxLng = loc.longitude
            }
        }
        
        region.center = CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2.0, longitude: (minLng + maxLng) / 2.0)
        
        region.span.latitudeDelta = (maxLat - minLat) * 1.15; // 15% padding
        region.span.longitudeDelta = (maxLng - minLng) * 1.35; // 13% padding
        
        return region;
    }
    
    //Retrieve location data to map the run route
    func arrangeOverlays() ->[MKPolyline]{
        var polylines = [MKPolyline]()
        let array = run?.locations
        
        for i in 1..<array!.count {
            
            let coorda = CLLocationCoordinate2D(latitude: array![i-1].latitude, longitude: array![i-1].longitude)
            let coordb = CLLocationCoordinate2D(latitude: array![i].latitude, longitude: array![i].longitude)
            
            var coords = [CLLocationCoordinate2D]()
            coords.append(coorda)
            coords.append(coordb)
            
            let polyline = MKPolyline(coordinates:&coords, count: 2)
            
            polylines.append(polyline)
        }
        return polylines
    }
    
    func arrangeAnnotations() ->[DistanceAnnotation]{
        var annotations = [DistanceAnnotation]()
        
        //Map the start point on map
        let locStart = run?.locations.first
        let annotationStart = DistanceAnnotation.init(myCoordinate: CLLocationCoordinate2D.init(latitude: locStart!.latitude, longitude: locStart!.longitude))
        annotationStart.image = UIImage.init(named: "start")
        annotations.append(annotationStart)
        
        if run?.distance < 1000.0 {
            let locEnd = run?.locations.last
            let annotationEnd = DistanceAnnotation.init(myCoordinate: CLLocationCoordinate2D.init(latitude: locEnd!.latitude, longitude: locEnd!.longitude))
            annotationEnd.image = UIImage.init(named: "end")
            annotations.append(annotationEnd)
            
            return annotations
        }
        
        var dis = 0.0
        var i = 1.0
        
        for loc in (run?.locations)! {
            dis += loc.perdistance
            if dis > 1000.0*i {
                let annotation = DistanceAnnotation.init(myCoordinate: CLLocationCoordinate2D.init(latitude: loc.latitude, longitude: loc.longitude))
                
                let label = UILabel.init(frame: CGRectMake(0, 0, 16, 16))
                label.textColor = UIColor.whiteColor()
                label.backgroundColor = UIColor.blackColor()
                label.textAlignment = .Center
                label.font = UIFont.systemFontOfSize(8)
                label.text = String(i)
                
                UIGraphicsBeginImageContextWithOptions(label.frame.size, false, 0.0);
                label.layer.renderInContext(UIGraphicsGetCurrentContext()!)
                annotation.image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                annotations.append(annotation)
                
                i += 1
            }
        }
        
        let locEnd = run?.locations.last
        let annotationEnd = DistanceAnnotation.init(myCoordinate: CLLocationCoordinate2D.init(latitude: locEnd!.latitude, longitude: locEnd!.longitude))
        annotationEnd.image = UIImage.init(named: "end")
        annotations.append(annotationEnd)
        
        return annotations
     }

}

//MARK: MKMapViewDelegate
extension RunDetailViewController:MKMapViewDelegate {
    
    //Set run route parameters
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {

        let polyLine = overlay as! MKPolyline
        let aRenderer = MKPolylineRenderer.init(polyline:polyLine)
        
        aRenderer.strokeColor = UIColor.blueColor()
        aRenderer.lineWidth = 3
        return aRenderer
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKindOfClass(DistanceAnnotation) {
            let distancePointAnnotation = annotation as! DistanceAnnotation
            var annView =  mapView.dequeueReusableAnnotationViewWithIdentifier("checkPoint")
            if (annView == nil) {
                annView = MKAnnotationView.init(annotation: distancePointAnnotation, reuseIdentifier: "checkPoint")
            }
            annView?.image = distancePointAnnotation.image
            return annView
        }else {
            return nil
        }
    }
}
