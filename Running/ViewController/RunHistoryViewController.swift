//
//  RunHistoryViewController.swift
//  Running
//
//  Created by zita on 16/3/17.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import Alamofire

class RunHistoryViewController: UIViewController {
    
    var animator: ZFModalTransitionAnimator!
    
    var tableView: UITableView?
    var dataSource = [Run]()
    var run:Run?

    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        tableView = UITableView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        
        tableView!.backgroundColor = UIColor.clearColor()
        
        tableView!.registerNib(UINib(nibName: "HistoryTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "resuseId")

        tableView!.delegate = self
        tableView!.dataSource = self
        
        view.insertSubview(tableView!, atIndex: 0)

        tableView?.tableFooterView = UIView()

        refreshControl.addTarget(self, action: #selector(loadWebData), forControlEvents: .ValueChanged)
        tableView!.addSubview(refreshControl)

        loadWebData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadWebData() {
        let token = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().token) as! String
        Alamofire.request(.GET, "https://r.nsswift.com/api/running/getall", parameters: nil, encoding: ParameterEncoding.JSON, headers: ["x-access-token": token]).responseJSON { (response) in

            if self.refreshControl.refreshing == true {
                self.refreshControl.endRefreshing()
                self.dataSource.removeAll()
            }

            switch response.result {
            case .Success(let value):
                if let data = value["data"] as? NSArray {
                    for item in data {
                        if let runDic = item as? NSDictionary {
                            let distance = runDic["distance"] as! Double
                            let time = runDic["time"] as! NSInteger
                            let date = runDic["date"] as! String
                            let calDouble = String(runDic["cal"]!)
                            let cal = String(format: "%.2f", Double(calDouble)!)


                            let array :[AnyObject] = (runDic["location"])! as! [AnyObject]
                            var tempArray = [location]()

                            for dic in array {
                                let altitude = Double(dic["altitude"] as! String) 
                                let latitude = Double(dic["latitude"] as! String) 
                                let longitude = Double(dic["longitude"] as! String) 
                                let perdistance = Double(dic["perdistance"] as! String)
                                let loc = location(altitude: altitude!, latitude: latitude!, longitude: longitude!, perdistance: perdistance!)
                                tempArray.append(loc)
                            }

                            self.dataSource.append(Run(distance: distance, time: time, locations: tempArray, date: date, cal: cal))
                        }
                    }

                    dispatch_async(dispatch_get_main_queue(), {
                        self.tableView?.reloadData()
                    })
                }
                print(value)
            case .Failure(_):
                let alertController = UIAlertController(title: "Failed", message: "No network connection.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension RunHistoryViewController:UITableViewDataSource, UITableViewDelegate {

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let springCell = tableView.dequeueReusableCellWithIdentifier("resuseId", forIndexPath: indexPath) as! HistoryTableViewCell
        
        let perRun = dataSource[indexPath.row]
        var dis = (perRun.distance/1000).format(perRun.distance/1000 > 10.0 ?".1":".2")
        dis += "Km"
        springCell.distanceLabel.text = dis
        springCell.timeLabel.text = stringSecondCount(perRun.time)

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")

        let date = dateFormatter.dateFromString(perRun.date)!

        let newDateFormat = NSDateFormatter()
        newDateFormat.dateFormat = "dd/MM/yy"
        let dateString = newDateFormat.stringFromDate(date)

        springCell.dateTimeLabel.text = dateString
        springCell.calLabel.text = perRun.cal + "Cal"

        return springCell
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 110
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let perRun = dataSource[indexPath.row]
        
        let runDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("runDetailViewController") as! RunDetailViewController
        
        //set transition animation
        animator = ZFModalTransitionAnimator(modalViewController: runDetailViewController)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.7
        self.animator.behindViewScale = 0.9
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Right
        
        runDetailViewController.transitioningDelegate = self.animator
        runDetailViewController.run = perRun
        
        self.presentViewController(runDetailViewController, animated: true) { () -> Void in
            
        }
        
    }
}
