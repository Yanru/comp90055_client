//
//  RunSettingTableViewController.swift
//  Running
//
//  Created by zita on 16/3/23.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import Alamofire

class RunSettingTableViewController: UITableViewController {

    @IBOutlet weak var gender: UISegmentedControl!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    
    var isHeight = false
    let userDefault = UserDefault()
    
    
    let defaultFrame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 280)
    
    let showFrame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height - 280, UIScreen.mainScreen().bounds.size.width, 280)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
        if NSUserDefaults.standardUserDefaults().objectForKey(userDefault.physiological.weight) != nil {
            weightTextField.text = NSUserDefaults.standardUserDefaults().stringForKey(userDefault.physiological.weight)
            heightTextField.text = NSUserDefaults.standardUserDefaults().stringForKey(userDefault.physiological.height)
            gender.selectedSegmentIndex = NSUserDefaults.standardUserDefaults().integerForKey(userDefault.physiological.gender) - 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
    }

    @IBAction func handleTapGesture(sender: AnyObject) {
        weightTextField.resignFirstResponder()
        heightTextField.resignFirstResponder()
    }
    // MARK: - SB Action

    @IBAction func updatePressed(sender: UIButton) {
        let token = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().token) as! String
        Alamofire.request(.POST, "https://r.nsswift.com/api/user/update", parameters: ["gender": gender.selectedSegmentIndex + 1, "weight": weightTextField.text!, "height": heightTextField.text!], encoding: ParameterEncoding.JSON, headers: ["x-access-token": token]).responseJSON { (response) in
            switch response.result {
            case .Success(let value):
                if let code = value["code"] as? NSInteger {
                    if code == 200 {
                        NSUserDefaults.standardUserDefaults().setInteger(self.gender.selectedSegmentIndex + 1, forKey: self.userDefault.physiological.gender)
                        NSUserDefaults.standardUserDefaults().setObject(self.heightTextField.text, forKey: self.userDefault.physiological.height)
                        NSUserDefaults.standardUserDefaults().setObject(self.weightTextField.text, forKey: self.userDefault.physiological.weight)

                        let alertController = UIAlertController(title: "Success", message: "The changes are saved.", preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        return
                    }else {
                        let alertController = UIAlertController(title: "Failed", message: value["msg"] as? String, preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        return
                    }
                }
            case .Failure(_):
                let alertController = UIAlertController(title: "Failed", message: "No network connection.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
        }
    }
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRectMake(0, 0, self.view.bounds.size.width, 20))
        view.backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1)
        return view
    }
}

extension RunSettingTableViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == weightTextField {
            if let w = Double(weightTextField.text!) {
                if w > 100 || w < 40 {
                    let alertController = UIAlertController(title: "Error", message: "Please enter a valid weight", preferredStyle: .Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    weightTextField.text = ""
                }
            }
        }

        if textField == heightTextField {
            if let w = Double(heightTextField.text!) {
                if w > 250 || w < 100 {
                    let alertController = UIAlertController(title: "Error", message: "Please enter a valid height", preferredStyle: .Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    heightTextField.text = ""
                }
            }
        }
    }
}
