//
//  RunningViewController.swift
//  Running
//
//  Created by zita on 16/3/6.
//  Copyright © 2016 Zita. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class RunningViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avgPaceLabel: UILabel!
    @IBOutlet weak var calLabel: UILabel!
    
    var countDownView:CountDownView?
    
    let locationManager = CLLocationManager.init()
    var pointAnnotation = MKPointAnnotation.init()
    
    var seconds = 0
    var distance = 0.0
    var locations = [CLLocation]()
    var locationsModelArray = [Dictionary<String, Double>]()
    
    var timer : NSTimer?
    
    deinit {
        print("run release")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMapAndLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //Initiate Countdown Timer
        countDownView = CountDownView.init(frame: self.view.bounds)
        countDownView!.countDownNumber = 3
        countDownView!.delegate = self
        countDownView!.start()
        
        self.view.addSubview(countDownView!)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: private
    
    func setMapAndLocation() {
        //map
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .Follow
        let region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 200, 200)
        mapView.setRegion(region, animated: true)
        
        //location
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.activityType = .Fitness
        locationManager.distanceFilter = 3
    }
    
    func startRun() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(RunningViewController.perSecond), userInfo: nil, repeats: true)
        locationManager.startUpdatingLocation()
       // NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
    }
    
    func perSecond() {
        seconds += 1
        
        var dis = (distance/1000).format(distance/1000 > 10.0 ?".1":".2")
        dis += "Km"
        distanceLabel.text = dis
        
        timeLabel.text = stringSecondCount(seconds)
        
        avgPaceLabel.text = stringAvgPace(distance, time: seconds)
        
        calLabel.text = calculateCal(distance, time: seconds).format(".1")
    }
    
    @IBAction func finishBtnPressed(sender: UIButton) {
        let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let actionDelete = UIAlertAction(title: "Delete & Quit", style: .Default) { (action:UIAlertAction) -> Void in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in})
        }
        
        alertController.addAction(actionCancel)
        alertController.addAction(actionDelete)
        
        if distance > 50 {
            let actionSave = UIAlertAction(title: "Save", style: .Default, handler: { (acion:UIAlertAction) -> Void in
                self.timer!.invalidate()
                self.locationManager.stopUpdatingLocation()
                
                self.uploadRunData()

            })
            alertController.addAction(actionSave)
        }
    
        self.presentViewController(alertController, animated: true) { () -> Void in  }
    }

    func uploadRunData() {
        let cal = calculateCal(self.distance, time: self.seconds).format(".1")

        var parameters: [String: AnyObject] = ["distance": distance, "time": seconds, "cal": cal]

        var locationArray: [Dictionary<String, Double>] = []
        for loc in locationsModelArray {
            let dict = ["altitude": loc["altitude"]!, "latitude": loc["latitude"]!, "perdistance": loc["perdistance"]!, "longitude": loc["longitude"]!]
            locationArray.append(dict)
        }
        parameters["location"] = locationArray

        let token = NSUserDefaults.standardUserDefaults().objectForKey(UserDefault().token) as! String
        Alamofire.request(.POST, "https://r.nsswift.com/api/running/add", parameters: parameters, encoding: ParameterEncoding.JSON, headers: ["x-access-token": token, "Content-Type": "application/json"]).responseJSON { (response) in
            switch response.result {
            case .Success(_):
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    NSNotificationCenter.defaultCenter().postNotificationName("RunningViewDismissed", object: nil)
                })
            case .Failure(_):

                let now = NSDate()
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let nowString = dateFormatter.stringFromDate(now)

                let cal = calculateCal(self.distance, time: self.seconds).format(".1")

                self.timer!.invalidate()
                self.locationManager.stopUpdatingLocation()

                let run = ["distance":self.distance,"time":self.seconds,"locations":self.locationsModelArray,"date":nowString,"cal":String(cal)]

                if !NSFileManager.defaultManager().fileExistsAtPath(dataFilePath()){
                    NSFileManager.defaultManager().createFileAtPath(dataFilePath(), contents: nil, attributes:nil)
                }

                run.writeToFile(dataFilePath(), atomically: true)

                let alertController = UIAlertController(title: nil, message: "No network connection. The run is saved locally.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in

                        NSNotificationCenter.defaultCenter().postNotificationName("RunningViewDismissed", object: nil)
                    })
                }))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}

//MARK: CountDownViewDelegate
extension RunningViewController:CountDownViewDelegate {
    func countDownFinished() {
        countDownView?.removeFromSuperview()
        countDownView = nil;
        startRun()
    }
}

//MARK: CLLocationManagerDelegate
extension RunningViewController:CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        for new in locations {
            let eventDate = new.timestamp
            let howRecent = eventDate.timeIntervalSinceNow
            
            //Filter locations within 10 seconds and 15 meters
            if howRecent < 10.0 && new.horizontalAccuracy < 15 {
                if self.locations.count > 0 {
                    
                    //Calculate the distance in between
                    let x = new.distanceFromLocation(self.locations.last!)
                    let y = fabs(new.altitude - self.locations.last!.altitude)
                    let dis = hypot(x, y)
                    distance += dis
                    
                    self.locationsModelArray.append(["altitude":new.altitude,"latitude":new.coordinate.latitude,"longitude":new.coordinate.longitude,"perdistance":dis])
                    
                    //Draw the route on map by connecting the current location to the last one using a straight line
                    var coords = [CLLocationCoordinate2D]()
                    coords.append(self.locations.last!.coordinate)
                    coords.append(new.coordinate)
                    
                    mapView.addOverlay(MKPolyline.init(coordinates: &coords, count: 2))
                }else {
                    self.locationsModelArray.append(["altitude":new.altitude,"latitude":new.coordinate.latitude,"longitude":new.coordinate.longitude,"perdistance":0])
                    
                }
                self.locations.append(new)
            }
        }
    }
}

//MARK: MKMapViewDelegate
extension RunningViewController:MKMapViewDelegate {
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        mapView.removeAnnotation(pointAnnotation)
        
        //Track moving annotation
        pointAnnotation.coordinate = userLocation.coordinate
        mapView.addAnnotation(pointAnnotation)
        
        //Set the location in the center of the map
        mapView.centerCoordinate = pointAnnotation.coordinate
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let polyLine = overlay as! MKPolyline
        let aRenderer = MKPolylineRenderer.init(polyline:polyLine)
        
        aRenderer.strokeColor = UIColor.blueColor()
        aRenderer.lineWidth = 3
        return aRenderer
    }
}


